
import dao.*;
import dates.*;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import model.Farmar;
import model.Jepartnerem;
import model.Trh;

import javax.swing.*;
import java.util.List;

public class MainController {

    daoFarm daoF = new daoFarm();
    daoTrh daoT = new daoTrh();
    daoJepartnerem daoJ = new daoJepartnerem();

    //**
    // Relationship Window Variables and methods------------------------------------------------
    // */

    //Left panel
    @FXML
    private Pane pane_vertical_relation;

    @FXML
    private Label label_relation;
    @FXML
    private TextField id_trh;

    @FXML
    private TextField id_farmar;

    @FXML
    private Button add_new_relat;

    @FXML
    private Button update_ok_relat;

    @FXML
    private Button update_cancel_relat;

    @FXML
    private Button view_trhy;

    @FXML
    private Button view_farmar;

    //Bottom panel
    @FXML
    private Pane pane_horizont_relation;

    @FXML
    private Button delete_relation;

    @FXML
    private Button load_data_relation_table;

    @FXML
    private Label lable_loading_data_relation_table;

    //Top panel
    @FXML
    private ScrollPane scroll_pane_relation;

    //Table relation
    @FXML
    private TableView<RelationData> relation_table;

    @FXML
    private TableColumn<RelationData, String> trh_column;

    @FXML
    private TableColumn<RelationData, String> farmar_column;

    //Methods

    //Ukáže stránku s data pro farmáři
    @FXML
    private void viewFarmar(ActionEvent event){
        cleanArchoPane();

        pane_vertical_farmar.setVisible(true);
        pane_horizont_farmar.setVisible(true);
        scroll_pane_farmar.setVisible(true);

    }

    //Ukáže stránku s data pro trhy
    @FXML
    private void viewTrhy(ActionEvent event){
        cleanArchoPane();

        pane_vertical_trh.setVisible(true);
        pane_horizont_trh.setVisible(true);
        scroll_pane_trh.setVisible(true);
    }

    //Tlačitko, po stisknutí vytvoří nový zaznám relation v database
    @FXML
    public void newRelation(ActionEvent event) {

        Jepartnerem p = new Jepartnerem(Integer.parseInt(id_trh.getText()), Integer.parseInt(id_farmar.getText()));

        if(daoT.findById(Integer.parseInt(id_trh.getText())) != null && daoF.findById(Integer.parseInt(id_farmar.getText())) != null){
            daoJ.save(p);
            id_trh.setText("");
            id_farmar.setText("");

            loadingDataRelation();
        }else{
            showErrorMsage("Invalid data, check and try again!");
        }

    }

    //proměnna, která uchovává vybraný v tabulce záznam relationship
    private RelationData selectionData = null;

    //odstraní vybraný v tabulce záznam z databaze
    @FXML
    public void deleteRelation(ActionEvent event) {

        if(selectionData != null){
            Jepartnerem j = new Jepartnerem(selectionData.getId_trh(), selectionData.getId_farmar());
            daoJ.delete(j);
            loadingDataRelation();
            selectionData = null;
        }else{
            showErrorMsage("Choose one of the options in the table!");
        }

    }

    // zavolá metodu pro uložení dat z database do tabulky
    @FXML
    public void loadDataRelationTable(ActionEvent event) {
        loadingDataRelation();
    }

    //ukaže formulař pro aktualizace vybraného záznamu
    @FXML
    public void updateRelation(ActionEvent event) {
        if (selectionData != null){
            id_trh.setText(Integer.toString(selectionData.getId_trh()));
            id_farmar.setText(Integer.toString(selectionData.getId_farmar()));

            label_relation.setText("Update relationship");
            add_new_relat.setVisible(false);
            view_farmar.setVisible(false);
            view_trhy.setVisible(false);
            pane_horizont_relation.setVisible(false);
            update_ok_relat.setVisible(true);
            update_cancel_relat.setVisible(true);
        }else{
            showErrorMsage("Choose one of the options in the table!");
        }

    }

    //tlačitko, která oznamuje systém, že uživatel podtverdí, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updateRelationPushOK(ActionEvent event) {

        Jepartnerem pNew = new Jepartnerem(Integer.parseInt(id_trh.getText()),Integer.parseInt(id_farmar.getText()));

        Jepartnerem pOld = new Jepartnerem(selectionData.getId_trh(),selectionData.getId_farmar());

        if (daoT.findById(Integer.parseInt(id_trh.getText())) != null
                && daoF.findById(Integer.parseInt(id_farmar.getText())) != null){
            daoJ.delete(pOld);
            daoJ.save(pNew);
            id_trh.setText("");
            id_farmar.setText("");
            loadingDataRelation();
            selectionData = null;

            label_relation.setText("New relationship");
            add_new_relat.setVisible(true);
            view_farmar.setVisible(true);
            view_trhy.setVisible(true);
            pane_horizont_relation.setVisible(true);
            update_ok_relat.setVisible(false);
            update_cancel_relat.setVisible(false);

        }else{
            showErrorMsage("Invalid data, check and try again!");
        }

    }

    //tlačitko, která oznamuje systém, že uživatel odmitne, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updateRelationPushCancel(ActionEvent event) {

        id_trh.setText("");
        id_farmar.setText("");
        loadingDataRelation();
        selectionData = null;

        label_relation.setText("New relationship");
        add_new_relat.setVisible(true);
        view_farmar.setVisible(true);
        view_trhy.setVisible(true);
        pane_horizont_relation.setVisible(true);
        update_ok_relat.setVisible(false);
        update_cancel_relat.setVisible(false);
    }

    //metoda pro uložení dat z database do tabulky
    public void loadingDataRelation(){

        TableView.TableViewSelectionModel<RelationData> selectionModel = relation_table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<RelationData>(){

            public void changed(ObservableValue<? extends RelationData> observable, RelationData oldValue, RelationData newValue) {
                if(newValue != null) selectionData = newValue;
            }

        });
        ObservableList<RelationData> data = FXCollections.observableArrayList();

        List<Jepartnerem> list = daoJ.findAll();

        for (Jepartnerem l : list){
            data.add(new RelationData(l.getId_farmar(), l.getId_trh()));
        }

        this.trh_column.setCellValueFactory(new PropertyValueFactory<RelationData, String>("id_trh"));
        this.farmar_column.setCellValueFactory(new PropertyValueFactory<RelationData, String>("id_farmar"));

        this.relation_table.setItems(data);
    }


    //**
    // Farmar Window Variables and methods----------------------------------------------
    // */

    //Left panel
    @FXML
    private Pane pane_vertical_farmar;

    @FXML
    private Label farmar_lable;

    @FXML
    private TextField id_new_farmar;

    @FXML
    private TextField name_new_farmar;

    @FXML
    private TextField email_new_farmar;

    @FXML
    private TextField phone_new_farmar;

    @FXML
    private TextField city_new_farmar;

    @FXML
    private Button add_new_farmar;

    @FXML
    private Button back_farmar;

    @FXML
    private Button ok_update_farmar;

    @FXML
    private Button canc_update_farmar;

    //Bottom panel
    @FXML
    private Pane pane_horizont_farmar;

    @FXML
    private Button update_farmar;

    @FXML
    private Button delete_farmar;

    //Top pane
    @FXML
    private ScrollPane scroll_pane_farmar;

    //Table farmar
    @FXML
    private TableView<FarmarData> farmar_table;

    @FXML
    private TableColumn<FarmarData, Integer> id_farmar_column;

    @FXML
    private TableColumn<FarmarData, String> name_farmar_column;

    @FXML
    private TableColumn<FarmarData, String> email_farmar_column;

    @FXML
    private TableColumn<FarmarData, String> phone_farmar_column;

    @FXML
    private TableColumn<FarmarData, String> city_farmar_column;

    //Methods

    //Tlačitko, po stisknutí vytvoří nový zaznám farmaři v database
    @FXML
    public void newFarmar(ActionEvent event) {
        Farmar f = new Farmar(Integer.parseInt(id_new_farmar.getText()), email_new_farmar.getText(), city_new_farmar.getText(), name_new_farmar.getText(), phone_new_farmar.getText());

        if(daoF.findById(Integer.parseInt(id_new_farmar.getText())) == null &&
                name_new_farmar.getText().length() <= 20 && phone_new_farmar.getText() != "" &&
                phone_new_trh.getText().length() <= 9 && city_new_farmar.getText().length() <= 64 &&
                email_new_farmar.getText().length() <= 64){
            daoF.save(f);
            id_new_farmar.setText("");
            email_new_farmar.setText("");
            city_new_farmar.setText("");
            name_new_farmar.setText("");
            phone_new_farmar.setText("");
            loadingDataFarmar();
        }else{
            showErrorMsage("Invalid data, check and try again!");
        }

    }

    //tlačitko pro vracení do main stránky s tabulkou relationships
    @FXML
    public void backFarmPush(ActionEvent event) {
        cleanArchoPane();

        pane_vertical_relation.setVisible(true);
        pane_horizont_relation.setVisible(true);
        scroll_pane_relation.setVisible(true);
    }

    //odstraní záznam vybraného farmaři
    @FXML
    public void deleteFarmar(ActionEvent event) {
        if(selectionDataFarmar != null){
            Farmar f = new Farmar(selectionDataFarmar.getId_new_farmar(), selectionDataFarmar.getEmail_new_farmar(),
                    selectionDataFarmar.getCity_new_farmar(), selectionDataFarmar.getName_new_farmar(), selectionDataFarmar.getPhone_new_farmar());
            List<Jepartnerem> partners = daoJ.findAll();
            for (Jepartnerem p: partners){
                if(p.getId_farmar() == selectionDataFarmar.getId_new_farmar()){
                    showErrorMsage("It is not possible to delete this object, because it is related to Trh in another table!");
                    return;
                }
            }
            daoF.delete(f);
            loadingDataFarmar();
            selectionDataFarmar = null;
        }else{
            showErrorMsage("Choose one of the options in the table!");

        }
    }

    //ukaže formulař pro aktualizace vybraného záznamu
    @FXML
    public void updateFarmar(ActionEvent event) {

        if(selectionDataFarmar != null){
            id_new_farmar.setText(Integer.toString(selectionDataFarmar.getId_new_farmar()));
            name_new_farmar.setText(selectionDataFarmar.getName_new_farmar());
            email_new_farmar.setText(selectionDataFarmar.getEmail_new_farmar());
            phone_new_farmar.setText(selectionDataFarmar.getPhone_new_farmar());
            city_new_farmar.setText(selectionDataFarmar.getCity_new_farmar());

            farmar_lable.setText("Update Farmar");
            add_new_farmar.setVisible(false);
            ok_update_farmar.setVisible(true);
            canc_update_farmar.setVisible(true);
            pane_horizont_farmar.setVisible(false);
            back_farmar.setVisible(false);

        }else{
            showErrorMsage("Choose one of the options in the table!");
        }

    }

    //tlačitko, která oznamuje systém, že uživatel podtverdí, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updatePushOkFarmar(ActionEvent event) {

        Farmar f = new Farmar(Integer.parseInt(id_new_farmar.getText()), email_new_farmar.getText(), city_new_farmar.getText(), name_new_farmar.getText(), phone_new_farmar.getText());

        if ((daoF.findById(Integer.parseInt(id_new_farmar.getText())).getId_farmar() == Integer.parseInt(id_new_farmar.getText()) &&
                name_new_farmar.getText().length() <= 20 && phone_new_farmar.getText() != "" &&
                phone_new_farmar.getText().length() <= 9 && city_new_farmar.getText().length() <= 64 &&
                email_new_farmar.getText().length() <= 64)){

            daoF.update(f);
            id_new_farmar.setText("");
            email_new_farmar.setText("");
            city_new_farmar.setText("");
            name_new_farmar.setText("");
            phone_new_farmar.setText("");
            loadingDataFarmar();
            selectionDataFarmar = null;
            farmar_lable.setText("New Farmar");
            add_new_farmar.setVisible(true);
            ok_update_farmar.setVisible(false);
            canc_update_farmar.setVisible(false);
            pane_horizont_farmar.setVisible(true);
            back_farmar.setVisible(true);
        }else{
            showErrorMsage("Invalid data, check and try again!");

        }

    }

    //tlačitko, která oznamuje systém, že uživatel odmitne, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updatePushCancelFarmar(ActionEvent event) {

        id_new_farmar.setText("");
        email_new_farmar.setText("");
        city_new_farmar.setText("");
        name_new_farmar.setText("");
        phone_new_farmar.setText("");
        selectionDataFarmar = null;
        loadingDataFarmar();

        farmar_lable.setText("New Farmar");
        add_new_farmar.setVisible(true);
        ok_update_farmar.setVisible(false);
        canc_update_farmar.setVisible(false);
        pane_horizont_farmar.setVisible(true);
        back_farmar.setVisible(true);
    }

    //zavolá metodu pro vložení dat z databaze do tabulky
    @FXML
    public void loadDataFarmarTable(ActionEvent event) {

        loadingDataFarmar();
    }

    //proměnna, která uchovává vybraný v tabulce záznam farmar
    private FarmarData selectionDataFarmar = null;

    //metoda pro vložení dat z databaze do tabulky
    public void loadingDataFarmar(){
        TableView.TableViewSelectionModel<FarmarData> selectionModel = farmar_table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<FarmarData>(){

            public void changed(ObservableValue<? extends FarmarData> observable, FarmarData oldValue, FarmarData newValue) {
                if (newValue != null) selectionDataFarmar = newValue;
            }

        });
        ObservableList<FarmarData> data = FXCollections.observableArrayList();

        List<Farmar> list = daoF.findAll();

        for (Farmar l : list){
            data.add(new FarmarData( l.getId_farmar(), l.getName(), l.getEmail(), l.getTelefon(), l.getMesto()));
        }


        this.id_farmar_column.setCellValueFactory(new PropertyValueFactory<FarmarData, Integer>("id_new_farmar"));
        this.name_farmar_column.setCellValueFactory(new PropertyValueFactory<FarmarData, String>("name_new_farmar"));
        this.email_farmar_column.setCellValueFactory(new PropertyValueFactory<FarmarData, String>("email_new_farmar"));
        this.phone_farmar_column.setCellValueFactory(new PropertyValueFactory<FarmarData, String>("phone_new_farmar"));
        this.city_farmar_column.setCellValueFactory(new PropertyValueFactory<FarmarData, String>("city_new_farmar"));

        this.farmar_table.setItems(data);
    }

    //**
    // Trh Window Variables and methods----------------------------------------------
    // */

    //Left panel
    @FXML
    private Pane pane_vertical_trh;

    @FXML
    private Label trh_lable;

    @FXML
    private TextField id_new_trh;

    @FXML
    private TextField name_new_trh;

    @FXML
    private TextField phone_new_trh;

    @FXML
    private TextField psc_new_trh;

    @FXML
    private TextField city_new_trh;

    @FXML
    private Button add_new_trh;

    @FXML
    private Button back_trh;

    @FXML
    private Button ok_update_trh;

    @FXML
    private Button canc_update_trh;

    //Bottom panel
    @FXML
    private Pane pane_horizont_trh;

    @FXML
    private Button update_trh;

    @FXML
    private Button delete_trh;

    //Top pane
    @FXML
    private ScrollPane scroll_pane_trh;

    //Table farmar
    @FXML
    private TableView<TrhData> trh_table;

    @FXML
    private TableColumn<TrhData, Integer> id_trh_column;

    @FXML
    private TableColumn<TrhData, String> name_trh_column;

    @FXML
    private TableColumn<TrhData, String> phone_trh_column;

    @FXML
    private TableColumn<TrhData, Integer> psc_trh_column;

    @FXML
    private TableColumn<TrhData, String> city_trh_column;

    private TrhData selectionDataTrh = null;

    //Methods

    //odstraní vybraný v tabulce záznam z database
    @FXML
    public void deleteTrh(ActionEvent event) {
        if(selectionDataTrh != null){
            Trh t = new Trh(selectionDataTrh.getId_new_trh(), selectionDataTrh.getCity_new_trh(), selectionDataTrh.getName_new_trh(),
                    selectionDataTrh.getPsc_new_trh(), selectionDataTrh.getPhone_new_trh());
            List<Jepartnerem> partners = daoJ.findAll();
            for (Jepartnerem p: partners){
                if(p.getId_trh() == selectionDataTrh.getId_new_trh()){
                    showErrorMsage("It is not possible to delete this object, because it is related to Farmar in another table!");
                    return;
                }
            }
            daoT.delete(t);
            loadingDataTrh();
            selectionDataFarmar = null;
        }else{
            showErrorMsage("Choose one of the options in the table!");
        }
    }


    //ukaže formulař pro aktualizace vybraného záznamu
    @FXML
    public void updateTrh(ActionEvent event) {

        if(selectionDataTrh != null){
            id_new_trh.setText(Integer.toString(selectionDataTrh.getId_new_trh()));
            name_new_trh.setText(selectionDataTrh.getName_new_trh());
            phone_new_trh.setText(selectionDataTrh.getPhone_new_trh());
            psc_new_trh.setText(Integer.toString(selectionDataTrh.getPsc_new_trh()));
            city_new_trh.setText(selectionDataTrh.getCity_new_trh());

            trh_lable.setText("Update Trh");
            add_new_trh.setVisible(false);
            pane_horizont_trh.setVisible(false);
            ok_update_trh.setVisible(true);
            canc_update_trh.setVisible(true);
            back_trh.setVisible(false);

        }else{
            showErrorMsage("Choose one of the options in the table!");
        }

    }

    //Tlačitko, po stisknutí vytvoří nový zaznám trhu v database
    @FXML
    public void newTrh(ActionEvent event) {
        Trh t = new Trh(Integer.parseInt(id_new_trh.getText()), city_new_trh.getText(), name_new_trh.getText(), Integer.parseInt(psc_new_trh.getText()), phone_new_trh.getText());

        if(daoT.findById(Integer.parseInt(id_new_trh.getText())) == null &&
                name_new_trh.getText() != "" && phone_new_trh.getText() != "" &&
                name_new_trh.getText().length() <= 20 && phone_new_trh.getText().length() <= 12 && city_new_trh.getText().length() <= 64){
            daoT.save(t);
            id_new_trh.setText("");
            city_new_trh.setText("");
            name_new_trh.setText("");
            psc_new_trh.setText("");
            phone_new_trh.setText("");
            loadingDataTrh();
        }else{
            showErrorMsage("Invalid data, check and try again!");
        }

    }

    //tlačitko pro vracení do main stránky s tabulkou relationships
    @FXML
    public void backTrhPush(ActionEvent event) {
        cleanArchoPane();

        pane_vertical_relation.setVisible(true);
        pane_horizont_relation.setVisible(true);
        scroll_pane_relation.setVisible(true);
    }

    //tlačitko, která oznamuje systém, že uživatel podtverdí, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updatePushOkTrh(ActionEvent event) {

        Trh t = new Trh(Integer.parseInt(id_new_trh.getText()), city_new_trh.getText(), name_new_trh.getText(), Integer.parseInt(psc_new_trh.getText()), phone_new_trh.getText());

        if (daoT.findById(Integer.parseInt(id_new_trh.getText())) != null &&
                daoT.findById(Integer.parseInt(id_new_trh.getText())).getId_trh() == selectionDataTrh.getId_new_trh() &&
                name_new_trh.getText() != "" && phone_new_trh.getText() != "" &&
                name_new_trh.getText().length() <= 20 && phone_new_trh.getText().length() <= 12
                && city_new_trh.getText().length() <= 64){

            daoT.update(t);
            id_new_trh.setText("");
            city_new_trh.setText("");
            name_new_trh.setText("");
            psc_new_trh.setText("");
            phone_new_trh.setText("");
            loadingDataTrh();
            selectionDataTrh = null;
            trh_lable.setText("New Trh");
            add_new_trh.setVisible(true);
            pane_horizont_trh.setVisible(true);
            ok_update_trh.setVisible(false);
            canc_update_trh.setVisible(false);
            back_trh.setVisible(true);
        }else{
            showErrorMsage("Invalid data, check and try again!");
        }

    }

    //tlačitko, která oznamuje systém, že uživatel odmitne, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updatePushCancelTrh(ActionEvent event) {
        System.out.println("Push cancel update trh");

        id_new_trh.setText("");
        city_new_trh.setText("");
        name_new_trh.setText("");
        psc_new_trh.setText("");
        phone_new_trh.setText("");

        loadingDataTrh();
        selectionDataTrh = null;

        trh_lable.setText("New Trh");
        add_new_trh.setVisible(true);
        pane_horizont_trh.setVisible(true);
        ok_update_trh.setVisible(false);
        canc_update_trh.setVisible(false);
        back_trh.setVisible(true);
    }

    //zavolá metodu pro vložení dat z databaze do tabulky
    @FXML
    public void loadDataTrhTable(ActionEvent event) {
        loadingDataTrh();
    }


    // metoda pro vložení dat z databaze do tabulky
    public void loadingDataTrh(){

        TableView.TableViewSelectionModel<TrhData> selectionModel = trh_table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<TrhData>(){

            public void changed(ObservableValue<? extends TrhData> observable, TrhData oldValue, TrhData newValue) {

                if (newValue != null) selectionDataTrh = newValue;
            }

        });

        ObservableList<TrhData> data = FXCollections.observableArrayList();

        List<Trh> list = daoT.findAll();

        for (Trh l : list){
            data.add(new TrhData( l.getId_trh(), l.getName(), l.getTelefon(), l.getPsc(), l.getMesto()));
        }

        this.id_trh_column.setCellValueFactory(new PropertyValueFactory<TrhData, Integer>("id_new_trh"));
        this.name_trh_column.setCellValueFactory(new PropertyValueFactory<TrhData, String>("name_new_trh"));
        this.phone_trh_column.setCellValueFactory(new PropertyValueFactory<TrhData, String>("phone_new_trh"));
        this.psc_trh_column.setCellValueFactory(new PropertyValueFactory<TrhData, Integer>("psc_new_trh"));
        this.city_trh_column.setCellValueFactory(new PropertyValueFactory<TrhData, String>("city_new_trh"));


        this.trh_table.setItems(data);
    }

    //Uklidí všechny elementy z ArdhoPane
    public void cleanArchoPane(){
        pane_vertical_relation.setVisible(false);
        pane_horizont_relation.setVisible(false);
        scroll_pane_relation.setVisible(false);

        pane_vertical_farmar.setVisible(false);
        pane_horizont_farmar.setVisible(false);
        scroll_pane_farmar.setVisible(false);

        pane_vertical_trh.setVisible(false);
        pane_horizont_trh.setVisible(false);
        scroll_pane_trh.setVisible(false);
    }


    //Okno Chyby
    private JTable jt;

    //Metoda, která přijímá zprávu o chybě a ukáže její
    public void showErrorMsage(String s){
        JOptionPane.showMessageDialog(new JTable(),s,"ERROR",JOptionPane.ERROR_MESSAGE);

    }
}
