package dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;

import java.util.List;

public class DAOClass<T> implements dao<T> {

    protected final Class<T> type;

    public DAOClass(Class<T> type) {
        this.type = type;
    }

    public T findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        T findT = session.get(type, id);
        session.close();
        return findT;
    }

    public void save(T e) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(e);
        tx1.commit();
        session.close();
    }

    public void update(T e) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(e);
        tx1.commit();
        session.close();
    }

    public void delete(T e) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(e);
        tx1.commit();
        session.close();
    }

    public List<T> findAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<T> users = (List<T>)session.createQuery("SELECT e FROM  " + type.getSimpleName() + " e").list();
        session.close();
        return users;
    }


}
