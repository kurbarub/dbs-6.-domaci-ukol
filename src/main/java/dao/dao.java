package dao;

import model.Farmar;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;

import java.util.List;

public interface dao<T> {

    /**
     * @param id
     * @return
     *
     * Vyhledá data v tabulce podle zadaného ID a vrátí celý záznam, pokud takové ID nenajde, vrátí null
     */
    T findById(int id);

    /**
     * @param e
     *
     * Přijímá nová data a ukládá je do databáze
     */
    void save(T e);

    /**
     * @param e
     *
     * Přijímá data a aktualizuje je v databázi
     */
    void update(T e);

    /**
     * @param e
     *
     * Odstraní data v databázi podle zadaných parametrů
     */
    void delete(T e);

    /**
     * @return
     *
     * Najděte všechny záznamy v databázové tabulce a vratí List těchto záznamů
     */
    List<T> findAll();

}