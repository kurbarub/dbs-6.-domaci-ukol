package dates;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FarmarData {

    private final SimpleIntegerProperty id_new_farmar;
    private final SimpleStringProperty name_new_farmar;
    private final SimpleStringProperty email_new_farmar;
    private final SimpleStringProperty phone_new_farmar;
    private final SimpleStringProperty city_new_farmar;

    public FarmarData(int id_new_farmar, String name_new_farmar,
                      String email_new_farmar, String phone_new_farmar,
                      String city_new_farmar) {
        this.id_new_farmar = new SimpleIntegerProperty(id_new_farmar);
        this.name_new_farmar = new SimpleStringProperty(name_new_farmar);
        this.email_new_farmar = new SimpleStringProperty(email_new_farmar);
        this.phone_new_farmar = new SimpleStringProperty(phone_new_farmar);
        this.city_new_farmar = new SimpleStringProperty(city_new_farmar);
    }

    public int getId_new_farmar() {
        return id_new_farmar.get();
    }

    public IntegerProperty id_new_farmarProperty() {
        return id_new_farmar;
    }

    public void setId_new_farmar(int id_new_farmar) {
        this.id_new_farmar.set(id_new_farmar);
    }

    public String getName_new_farmar() {
        return name_new_farmar.get();
    }

    public StringProperty name_new_farmarProperty() {
        return name_new_farmar;
    }

    public void setName_new_farmar(String name_new_farmar) {
        this.name_new_farmar.set(name_new_farmar);
    }

    public String getEmail_new_farmar() {
        return email_new_farmar.get();
    }

    public StringProperty email_new_farmarProperty() {
        return email_new_farmar;
    }

    public void setEmail_new_farmar(String email_new_farmar) {
        this.email_new_farmar.set(email_new_farmar);
    }

    public String getPhone_new_farmar() {
        return phone_new_farmar.get();
    }

    public StringProperty phone_new_farmarProperty() {
        return phone_new_farmar;
    }

    public void setPhone_new_farmar(String phone_new_farmar) {
        this.phone_new_farmar.set(phone_new_farmar);
    }

    public String getCity_new_farmar() {
        return city_new_farmar.get();
    }

    public StringProperty city_new_farmarProperty() {
        return city_new_farmar;
    }

    public void setCity_new_farmar(String city_new_farmar) {
        this.city_new_farmar.set(city_new_farmar);
    }
}
