package dates;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class RelationData {

    private final SimpleIntegerProperty id_farmar;
    private final SimpleIntegerProperty id_trh;

    public RelationData(int id_farmar, int id_trh) {
        this.id_farmar = new SimpleIntegerProperty(id_farmar);
        this.id_trh = new SimpleIntegerProperty(id_trh);
    }

    public int getId_farmar() {
        return id_farmar.get();
    }

    public IntegerProperty id_farmarProperty() {
        return id_farmar;
    }

    public void setId_farmar(int id_farmar) {
        this.id_farmar.set(id_farmar);
    }

    public int getId_trh() {
        return id_trh.get();
    }

    public IntegerProperty id_trhProperty() {
        return id_trh;
    }

    public void setId_trh(int id_trh) {
        this.id_trh.set(id_trh);
    }
}
