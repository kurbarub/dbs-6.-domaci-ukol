package model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "trh")
public class Trh {

    @Id
    private int id_trh;

    @Column(name = "mesto")
    private String mesto;

    @Column(name = "nazev")
    private String name;

    @Column(name = "psc")
    private int psc;

    @Column(name = "telefon")
    private String telefon;

    @ManyToMany
    @JoinTable(name = "jepartnerem",joinColumns = @JoinColumn(name ="id_trh"),inverseJoinColumns = @JoinColumn(name = "id_farmar"))
    private Collection<Farmar> farmari;

    public Trh(){

    }

    public Trh(int id_trh, String mesto, String name, int psc, String telefon) {
        this.mesto = mesto;
        this.name = name;
        this.psc = psc;
        this.telefon = telefon;
        this.id_trh = id_trh;
    }

    public int getId_trh() {
        return id_trh;
    }

    public void setId_trh(int id_trh) {
        this.id_trh = id_trh;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPsc() {
        return psc;
    }

    public void setPsc(int psc) {
        this.psc = psc;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Collection<Farmar> getFarmari() {
        return farmari;
    }

    public void setFarmari(Collection<Farmar> farmari) {
        this.farmari = farmari;
    }

}
