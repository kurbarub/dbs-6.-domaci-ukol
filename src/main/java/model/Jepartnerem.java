package model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "jepartnerem")
public class Jepartnerem implements Serializable {

    @Id
    private Integer id_trh;

    @Id
    private Integer id_farmar;

    public Jepartnerem(){

    }

    public Jepartnerem(Integer id_trh, Integer id_farmar) {
        this.id_trh = id_trh;
        this.id_farmar = id_farmar;
    }

    public Integer getId_trh() {
        return id_trh;
    }

    public void setId_trh(int id_trh) {
        this.id_trh = id_trh;
    }

    public Integer getId_farmar() {
        return id_farmar;
    }

    public void setId_farmar(Integer id_farmar) {
        this.id_farmar = id_farmar;
    }

}
